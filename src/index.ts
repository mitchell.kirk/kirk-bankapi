import express from 'express';
import BankService from './services/bank-service';
import { BankServiceImpl } from './services/bank-service-impl';
import { Account, Client } from './entities';
import { MissingResourceError } from './errors';
import { InsufficientFundsError } from './errors';

const app = express();
app.use(express.json());

const bankService:BankService = new BankServiceImpl();

app.get("/clients", async (req, res) =>{
    const clients:Client[] = await bankService.retrieveAllClients();
    res.status(200);
    res.send(clients);
});

app.get("/clients/:id", async (req, res) =>{
    try{
    const clientId = Number(req.params.id);
    const client:Client = await bankService.retrieveClientById(clientId);
    res.status(200);
    res.send(client);
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.post("/clients", async (req, res) =>{
    let client:Client = req.body;
    client = await bankService.registerClient(client);
    res.status(201);
    res.send(client);

});

app.put("/clients/:id", async (req, res) =>{
    try{
    let client:Client = req.body;
    client = await bankService.modifyClient(client);
    res.status(200);
    res.send(client);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        } 
    }
});

app.delete("/clients/:id", async (req, res) =>{
    try {
    const clientId = Number(req.params.id);
    const boolean = await bankService.removeClient(clientId);
    res.status(205);
    res.send(boolean);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.post("/clients/:id/accounts", async (req, res) =>{
    let account:Account = req.body;
    account.clientId = Number(req.params.id);
    account = await bankService.createNewAccount(account);
    res.status(201);
    res.send(account);
});

app.get("/clients/:id/accounts", async (req, res) =>{
    try{
    const clientId = Number(req.params.id)
    const accounts:Account[] = await bankService.retrieveAllAccounts(clientId);
    res.status(200);
    res.send(accounts)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.get("/accounts/:id", async (req, res) =>{
    try{
    const accNum = Number(req.params.id)
    const account:Account = await bankService.retrieveAccountById(accNum);
    res.status(200);
    res.send(account)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.put("/accounts/:id", async (req, res) =>{
    try{
    let account:Account = req.body;
    account.accNum = Number(req.params.id);
    account = await bankService.modifyAccount(account);
    res.status(200);
    res.send(account);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.delete("/accounts/:id", async (req, res) =>{
    try{
    const accNum = Number(req.params.id);
    const boolean = await bankService.removeAccount(accNum);
    res.status(200);
    res.send(boolean);
    } catch (error){
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.patch("/accounts/:id/deposit", async (req, res) =>{
    try{
    const accNum = Number(req.params.id);
    const deposit = Number(req.body.amount);
    const account:Account = await bankService.depositToAccount(accNum, deposit);
    res.status(200);
    res.send(account)
    } catch (error) {
        if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.patch("/accounts/:id/withdraw", async (req, res) =>{
    try{
    const accNum = Number(req.params.id);
    const withdraw = Number(req.body.amount);
    const balance = await bankService.withdrawFromAccount(accNum, withdraw);
    res.status(200);
    res.send(`Transaction complete. New balance ${balance}`)
    } catch (error) {
        if (error instanceof InsufficientFundsError){
            res.status(422);
            res.send(error)
        } else if (error instanceof MissingResourceError){
            res.status(404);
            res.send(error)
        }
    }
});

app.listen(3000, () => console.log("Application Started..."));