import { Client} from "../entities";
import { Account } from "../entities";


export default interface BankService{
    //create
    registerClient(client:Client):Promise<Client>;
    createNewAccount(account:Account):Promise<Account>

    //read
    retrieveAllClients():Promise<Client[]>;
    retrieveClientById(clientId:number):Promise<Client>;
    retrieveAllAccounts(clientId:number):Promise<Account[]>;
    retrieveAccountById(accNum:number):Promise<Account>;

    //modify
    modifyAccount(account:Account):Promise<Account>
    modifyClient(client:Client):Promise<Client>
    depositToAccount(accNum:number, deposit:number):Promise<Account>;
    withdrawFromAccount(accNum:number, withdraw:number):Promise<string>;


    //delete
    removeClient(clientId:number):Promise<boolean>
    removeAccount(accNum:number):Promise<boolean>
}