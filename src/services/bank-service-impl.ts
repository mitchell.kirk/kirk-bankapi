import { AccountDAO, ClientDAO } from "../daos/bank-dao";
import { BankDaoPostgres } from "../daos/bank-dao-postgres";
import { Client } from "../entities";
import { Account } from "../entities";
import BankService from "./bank-service";



export class BankServiceImpl implements BankService{
 

    clientDAO:ClientDAO = new BankDaoPostgres();
    accountDAO:AccountDAO = new BankDaoPostgres();

    depositToAccount(accNum: number, deposit:number): Promise<Account> {
        return this.accountDAO.makeDeposit(accNum, deposit)
    }
    async withdrawFromAccount(accNum: number, withdraw: number): Promise<string> {
        let result = await this.accountDAO.makeWithdrawal(accNum, withdraw)
        if(result === -1){
            return "Insufficient funds";
        } else return result.toString();
    }
    registerClient(client: Client): Promise<Client> {
        return this.clientDAO.createClient(client);
    }
    createNewAccount(account: Account): Promise<Account> {
        return this.accountDAO.createAccount(account);
    }
    retrieveAllClients(): Promise<Client[]> {
        return this.clientDAO.getAllClients();
    }
    retrieveClientById(clientId: number): Promise<Client> {
        return this.clientDAO.getClientById(clientId);
    }
    retrieveAllAccounts(clientId: number): Promise<Account[]> {
        return this.accountDAO.getAllAccounts(clientId);
    }
    retrieveAccountById(accNum: number): Promise<Account> {
        return this.accountDAO.getAccountsById(accNum);
    }
    modifyAccount(account: Account): Promise<Account> {
        return this.accountDAO.updateAccount(account);
    }
    modifyClient(client: Client): Promise<Client> {
        return this.clientDAO.updateClient(client);
    }
    removeClient(clientId: number): Promise<boolean> {
        return this.clientDAO.deleteClientById(clientId);
    }
    removeAccount(accNum: number): Promise<boolean> {
        return this.accountDAO.deleteAccountById(accNum);
    }  
}