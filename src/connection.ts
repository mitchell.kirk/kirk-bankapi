import { Client } from "pg"; 
require('dotenv').config({path: 'C:\\Users\\Mitchell\\Desktop\\BankingAPI\\app.env'});

export const conn = new Client({
    user:'postgres',
    password:process.env.BANKINGAPIPW, 
    database:process.env.DATABASENAME,
    port:5432,
    host:'34.139.115.50'
});
conn.connect();
