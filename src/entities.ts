

export class Client { 
    constructor(
        public clientId:number,
        public fName:string,
        public lName:string
    ){}
}

export class Account { 
    constructor(
        public accNum:number,
        public clientId:number,
        public balance:number,
        public accType:string
    ){}
}