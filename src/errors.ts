
export class MissingResourceError{

    message:string;
    description:string = "Sorry, the resource you requested could not be found";

    constructor(message:string){
        this.message = message;
    }
}


export class InsufficientFundsError{

    message:string;
    description:string = "Insufficient Funds";

    constructor(message:string){
        this.message = message;
    }
}

export class CreationError{

    message:string;
    description:string = "Error creating element";

    constructor(message:string){
        this.message = message;
    }
}