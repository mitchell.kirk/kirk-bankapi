
import { validateLocaleAndSetLanguage } from "typescript";
import { conn } from "../connection";
import { Client } from "../entities";
import { Account } from "../entities";
import { InsufficientFundsError, MissingResourceError } from "../errors";
import { AccountDAO, ClientDAO } from "./bank-dao";



export class BankDaoPostgres implements ClientDAO, AccountDAO{



    async createClient(client: Client): Promise<Client> {
        const sql:string = "INSERT INTO client(client_id,f_name,l_name) VALUES ($1,$2,$3) RETURNING client_id"; 
        const values = [client.clientId,client.fName,client.lName];
        const result = await conn.query(sql,values);
        client.clientId = result.rows[0].client_id;
        return client;
    }
    async getAllClients(): Promise<Client[]> {
        const sql:string = "SELECT * FROM client";
        const result = await conn.query(sql);
        const clients:Client[] = [];
        for(const row of result.rows){
            const client:Client = new Client(
                row.client_id,
                row.f_name,
                row.l_name);
            clients.push(client);
        }
        return clients;
    }
    async getClientById(clientId: number): Promise<Client> {
        const sql:string = "SELECT * FROM client WHERE client_id = $1 ";
        const values = [clientId]
        const result = await conn.query(sql,values);
        if(result.rowCount ===0){
            throw new MissingResourceError(`The client with ID ${clientId} could not be found.`)
        }
        const row = result.rows[0]
        const client:Client = new Client(
            row.client_id,
            row.f_name,
            row.l_name);
        return client;
        
    }
    async updateClient(client: Client): Promise<Client> {
        const sql:string = "UPDATE client SET f_name=$1, l_name=$2 WHERE client_id=$3";
        const values = [client.fName,client.lName,client.clientId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with Id of ${client.clientId} could not be found.`)
        }
        return client;
    }
    async deleteClientById(clientId: number): Promise<boolean> {
        const sql:string = "DELETE FROM client WHERE client_id=$1";
        const values = [clientId]
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with Id of ${clientId} could not be found.`)
        }
        return true;
    }
    async createAccount(account: Account): Promise<Account> {
        const sql:string = "INSERT INTO account(acc_num,client_id,balance,acc_type) values ($1,$2,$3,$4)";
        const values = [account.accNum,account.clientId,account.balance,account.accType];
        const result = await conn.query(sql, values);
        return account;
    }
    async getAccountsById(accNum: number): Promise<Account> {
        const sql:string = "SELECT * FROM account WHERE acc_num =$1";
        const values = [accNum];
        const result = await conn.query(sql, values);
        if(result.rowCount ===0){
            throw new MissingResourceError(`The account with account number of ${accNum} could not be found.`)
        }
        const row = result.rows[0]
        const account:Account = new Account(
            row.acc_num,
            row.client_id,
            row.balance,
            row.acc_type);
        return account;
    }
    getAccountsByType(accountType: string): Promise<Account> {
        throw new Error("Method not implemented.");
    }
    async getAllAccounts(clientId: number): Promise<Account[]> {
        const sql:string = "SELECT * FROM account WHERE client_id =$1";
        const values = [clientId];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No accounts could be found for client with Id of ${clientId}.`)
        }
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
                row.acc_num,
                row.client_id,
                row.balance,
                row.acc_type);
            accounts.push(account);
        }
        return accounts;
    }
    async updateAccount(account: Account): Promise<Account> {
        const sql:string = "UPDATE account SET client_id=$1, balance=$2, acc_type=$3 WHERE acc_num=$4";
        const values = [account.clientId,account.balance,account.accType,account.accNum];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No account could be found for ${account.clientId} with account number ${account.accNum}.`)
        }
        return account;
    }
    async deleteAccountById(accNum: number): Promise<boolean> {
        const sql:string = "DELETE FROM account WHERE acc_num = $1";
        const values = [accNum];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No account could be found with account number ${accNum}.`)
        }
        return true;
    }
    async makeDeposit(accNum: number, deposit: number): Promise<Account> {
        const sql:string = "UPDATE account SET balance=balance+$1 WHERE acc_num=$2 RETURNING acc_num,client_id,balance,acc_type";
        const values = [deposit,accNum];
        const result = await conn.query(sql, values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`No account could be found with account number ${accNum}.`)
        }
        const row = result.rows[0]
        const account:Account = new Account(
            row.acc_num,
            row.client_id,
            row.balance,
            row.acc_type);
        return account;
    }
    async makeWithdrawal(accNum: number, withdraw: number): Promise<number> {
        const sql:string = "SELECT withdrawal($1,$2)";
        const values = [accNum,withdraw];
        const result = await conn.query(sql, values);
        const row = result.rows[0]
        if(result.rowCount === 0 || row.withdrawal === -2){
            throw new MissingResourceError(`No account could be found with account number ${accNum}.`)
        } 
        if(row.withdrawal === -1){
            throw new InsufficientFundsError(`Insufficent funds available in account ${accNum}.`)
        } 
        const balance = row.withdrawal;
        return balance;
    }
}