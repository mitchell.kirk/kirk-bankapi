import { Client } from "../entities";//chaanging for debugging
import { Account } from "../entities";

export interface ClientDAO{
    //create
    createClient(client:Client):Promise<Client>;

    //read
    getAllClients():Promise<Client[]>;
    getClientById(clientId:number): Promise<Client>;

    //update
    updateClient(client:Client):Promise<Client>;

    //delete
    deleteClientById(clientId:number):Promise<boolean>;
}

export interface AccountDAO{
    //create
    createAccount(account:Account):Promise<Account>;

    //read
    getAccountsById(accNum:number): Promise<Account>;
    getAccountsByType(accountType:string): Promise<Account>;
    getAllAccounts(clientId: number):Promise<Account[]>;
    
    //update
    updateAccount(account:Account):Promise<Account>;
    makeDeposit(accNum: number, deposit: number):Promise<Account>;
    makeWithdrawal(accNum: number, withdraw: number):Promise<number>;
    
    //delete
    deleteAccountById(accNum:number):Promise<boolean>;
}