import { Client } from "../src/entities";
import { Account } from "../src/entities";
import BankService from "../src/services/bank-service";
import { BankServiceImpl } from "../src/services/bank-service-impl";
import { conn } from "../src/connection";

const bankService:BankService = new BankServiceImpl();

const testClient:Client = new Client(100,"Luke","Skywalker");
const del:string = "drop table client, account cascade";
const resetC:string = "create table client( client_id int primary key, f_name varchar(200), l_name varchar(200));";
const resetA:string = "create table account(acc_num int primary key,client_id int,balance int,acc_type varchar(200),constraint fk_client_id foreign key (client_id) references client(client_id) on delete cascade);";

async function resetDatabase(){
    conn.query(del);
    conn.query(resetC);
    conn.query(resetA);
}
//conn.on;
resetDatabase();

test("Create a client", async () =>{
    const result:Client = await bankService.registerClient(testClient)
    expect(result.clientId).toBe(100);
    await resetDatabase();
});

test("Get all clients", async () =>{
    let client1:Client = new Client(101,"jest","jest");
    let client2:Client = new Client(102,"jest","jest");
    let client3:Client = new Client(103,"jest","jest");
    await bankService.registerClient(client1)
    await bankService.registerClient(client2)
    await bankService.registerClient(client3)

    const clients:Client[] = await bankService.retrieveAllClients();
    expect(clients.length).toBe(3);

    await resetDatabase();
});

test ("Get client by Id", async () =>{
    let client:Client = new Client(99, "James", "Kirk");
    client = await bankService.registerClient(client);
    let retrievedClient:Client = await bankService.retrieveClientById(client.clientId);

    expect(retrievedClient.clientId).toBe(99);

    await resetDatabase();
});

test("Update clients", async () =>{
    let client:Client = await bankService.registerClient(testClient)
    let updatedClient:Client = new Client(testClient.clientId,"Darth","Vader");
    updatedClient = await bankService.modifyClient(updatedClient);

    expect(updatedClient.clientId).toBe(100)
    expect(updatedClient.fName).toBe("Darth")
    expect(updatedClient.lName).toBe("Vader")

    await resetDatabase();
});

test("Delete a client", async () =>{
    const client:Client = await bankService.registerClient(testClient)
    const result = await bankService.removeClient(client.clientId);

    expect(result).toBe(true);

    await resetDatabase();
});

test("Create an account", async () =>{
    const client:Client = await bankService.registerClient(testClient);
    const account:Account = new Account(1,client.clientId,2,"test");
    const result = await bankService.createNewAccount(account);
    
    expect(result.accNum).toBe(1);
    expect(result.clientId).toBe(100);
    expect(result.balance).toBe(2);
    expect(result.accType).toBe("test");

    await resetDatabase();
});

test("Get all accounts", async () =>{
    const client:Client = await bankService.registerClient(testClient)
    let account1:Account = new Account(1,client.clientId,2,"test");
    let account2:Account = new Account(2,client.clientId,2,"test2");
    account1 = await bankService.createNewAccount(account1);
    account2 = await bankService.createNewAccount(account2);
    const accounts:Account[] = await bankService.retrieveAllAccounts(client.clientId);

    expect(accounts.length).toBe(2);

    await resetDatabase();
});

test ("Get account by account number", async () =>{
    const testClient:Client = new Client(100,"Luke","Skywalker");
    await bankService.registerClient(testClient)
    const account:Account = new Account(1,testClient.clientId,3,"test");
    let result:Account = await bankService.createNewAccount(account);
    result = await bankService.retrieveAccountById(account.accNum)

    expect(result.accNum).toBe(1)
    await resetDatabase();
});

test("Update account", async () =>{
    const testClient:Client = new Client(100,"Luke","Skywalker");
    await bankService.registerClient(testClient)
    const account:Account = new Account(1,testClient.clientId,3,"test");
    await bankService.createNewAccount(account);
    const alteredAccount:Account = new Account(1,testClient.clientId,4,"test2");
    const result = await bankService.modifyAccount(alteredAccount)

    expect(result.accNum).toBe(1)
    expect(result.clientId).toBe(testClient.clientId)
    expect(result.balance).toBe(4)
    expect(result.accType).toBe("test2")

    await resetDatabase();
});

test("Delete account", async () =>{
    const testClient:Client = new Client(100,"Luke","Skywalker");
    await bankService.registerClient(testClient)
    const account:Account = new Account(1,testClient.clientId,2,"test");
    await bankService.createNewAccount(account);
    const result = await bankService.removeAccount(account.accNum);

    expect(result).toBe(true);

    await resetDatabase();
});

test("Deposit", async () =>{
    const testClient:Client = new Client(100,"Luke","Skywalker");
    await bankService.registerClient(testClient)
    const account:Account = new Account(1,testClient.clientId,2,"test");
    await bankService.createNewAccount(account);
    const result = await bankService.depositToAccount(account.accNum, 20);

    expect(result.balance).toBe(22);

    await resetDatabase();
});

test("Withdraw", async () =>{
    const testClient:Client = new Client(100,"Luke","Skywalker");
    await bankService.registerClient(testClient)
    const account:Account = new Account(1,testClient.clientId,20,"test");
    await bankService.createNewAccount(account);
    const result = await bankService.withdrawFromAccount(account.accNum, 10);

    expect(result).toBe("10");

    await resetDatabase();
});

afterAll(()=>{
    conn.end();
});
